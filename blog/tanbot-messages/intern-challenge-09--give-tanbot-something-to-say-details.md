# Challenge 9: Give Tanbot Something to Say

#### _`date_scheduled = datetime(2021, 3, 20, 30, 0)`_

In [this challenge](gitlab.com/tangibleai/team/-/blob/master/src/team/interns/2021_winter/intern-challenges/intern-challenge-09--give-tanbot-something-to-say.md) you will go behind the scenes to help Tanbot perform its chatbot stunt.

Here's the Django admin interface for Tanbot: [staging.qary.ai/admin]
If you haven't received the credentials (username, password, or tokens) you need, send @hobs or "@Dwayne Negron" a message on Slack

## Instructions

To practice "active learning":

1. Click one of the tables that seems interesting from among those listed on the Tanbot admin interface
2. Click the plus sign to add a new row to the table
3. Check out the form and see if it contains all the columns (fields) you think it should. Don't add anything just yet, just click the back button in your browser.
4. Send a message on Slack to suggest an improvement, or to ask a question about anything you see that looks weird. 
5. Think back to that database table and the Create Record web form you looked at. Try to imagine what the Python class would look like in the django `models.py` file.
6. Here's the models.py file for that table that you looked at: [gitlab.com/tangibleai/tanbot/-/blob/master/src/onboard/models.py] or [gitlab.com/tangibleai/tanbot/-/tree/master/src/tanbot/models.py]
7. Think about what's different about what you see in models.py and what you expected to see. Jot down your thoughts in a note somewhere. You'll need that in a second.
9. Go back to the Django admin interface for Tanbot and log in.
10. Look for the Message table.
11. Within the Message table click on the plus icon to create a new Message record.
12. Set the title for your message to "broadcast".
13. Schedule it to go out tomorrow at the same time of day that you are doing this exercise. To do this you'll need to use the calendar and clock widgets next to the "Date Scheduled" field.
14. In the text box put your "Message in a Bottle". Keep in mind it will go out to all the other interns on Slack. You'd like to give them something interesting to think about, so put something that you learned about Django models.py files or the Tanbot chatbot in your message.
15. Click the blue Save button at the bottom
16. That's it! Mission accomplished!
17. If you still have energy, visit the Tanbot repository at [gitlab.com/tangibleai/Tanbot] and add yourself to the CONTRIBUTORS.md file!
