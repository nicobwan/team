# Finding and Parsing Tables on Wikipedia

```python
base_url = 'https://en.wikipedia.org'
search_text = 'hugo award best novel liu'
```

```python
import requests
page = requests.get(
    base_url,
    {'search': search_text},
)
page
```

```python
import bs4
soup = bs4.BeautifulSoup(page.text)
```

```python
>>> hugo_url = soup.find('div', {'class': 'searchresults'}).find('ul').find('li').find('a').get('href')
>>> hugo_url
'/wiki/Hugo_Award_for_Best_Novel'
```

If you wanted to parse it manually
```python
>>> hugo_url = '/'.join((base_url, hugo_url))
>>> page = requests.get(base_url,
...     {'search': search_text})
>>> page
<Response [200]>
```

```python
import pandas as pd
pd.read_html(hugo_url)
```
